from db.model import db,app, products, granites,granitethick,granitephoto, thick
from flask import abort, Response, json, jsonify
from routes import upphoto
from flask_jwt_extended import jwt_required, current_user
from trends.trend import Trending

class Marbles:
    # def __init__(self, product = None, thik = None, granite = None, file = None, trend = None, description = None):
    #     self.product = product
    #     self.thik   = thik
    #     self.granite = granite
    #     self.file = file
    #     self.trend = trend
    #     self.description = description
    @jwt_required()
    def post_granites(self, dataList):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401

        file_data = dataList.pop()
        file = file_data.get('file')
        url = upphoto.upload_photo(file, "granitephotos")
        image_url = url["url"]
        file_id = url["file_id"]
        photo_address = image_url

        results = []
        with app.app_context():
            for data in dataList:
                product = data.get('product')
                granite = data.get('granite')
                thik = data.get('thick')
                description = data.get('description')
                trend = data.get('trending')

                try:
                    # Validate product, granite, and thick
                    for item, model, attr in [
                        (product, products, 'productname'),
                        (granite, granites, 'category'),
                        (thik, thick, 'thick')
                    ]:
                        query = model.query.filter_by(**{ attr: item }).first()
                        if not query:
                            raise ValueError(f"{item} not found")

                    tile_type = granitethick.query.join(products).join(granites).join(thick).filter(
                        products.productname == product,
                        granites.category == granite,
                        thick.thick == thik
                    ).first()

                    if not tile_type:
                        raise ValueError("Combination not found")

                    upload_photo = granitephoto(
                        photoaddress = photo_address, gtid = tile_type.gtid, imagekitid = file_id,
                        description = description
                    )
                    db.session.add(upload_photo)
                    db.session.commit()

                    result = { "message": "Photo uploaded successfully", "product": product, "granite": granite,
                               "thick": thik }
                    if trend == "True":
                        trending_product = Trending()
                        trend_response = trending_product.post_trending_products(photourl = photo_address)
                        result["trending"] = trend_response

                    results.append(result)

                except Exception as e:
                    results.append({ "error": str(e), "product": product, "granite": granite, "thick": thik })

        return jsonify(results)

    def get_granites(self, granite, thik):
        with app.app_context():
            if granite and granite.strip():
                granite = granite.strip()
                is_granite = granites.query.filter_by(category = granite).first()
                if not is_granite:
                    abort(404)

            if thik and thik.strip():
                thik = thik.strip()
                is_thick = thick.query.filter_by(thick = thik).first()
                if not is_thick:
                    abort(404)

            try:
                details = []
                seen_photos = set()

                # Case: both granite and thick provided
                if granite and thik:
                    url = granitethick.query.join(granites).join(thick).filter(
                        granites.category == granite,
                        thick.thick == thik
                    ).all()

                    for row in url:
                        for photo in row.photoaddress:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": thik,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                # Case: only thick provided
                elif not granite and thik:
                    url = granitethick.query.join(thick).filter(thick.thick == thik).all()
                    for row in url:
                        for photo in row.photoaddress:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": thik,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                # Case: only granite provided
                elif granite and not thik:
                    url = granitephoto.query.join(granitethick).join(granites).filter(
                        granites.category == granite
                    ).all()
                    for row in url:
                        if row.photoaddress not in seen_photos:
                            size = thick.query.join(granitethick).join(granitephoto).filter(
                                granitephoto.gtid == row.gtid
                            ).first()
                            if size:
                                ls = {
                                    "size": size.thick,
                                    "url": row.photoaddress
                                }
                                if row.description:
                                    ls["description"] = row.description
                                details.append(ls)
                                seen_photos.add(row.photoaddress)

                # Case: neither granite nor thick provided
                elif not granite and not thik:
                    url = granitephoto.query.join(granitethick).all()
                    for row in url:
                        if row.photoaddress not in seen_photos:
                            size = thick.query.join(granitethick).join(granitephoto).filter(
                                granitephoto.gtid == row.gtid
                            ).first()
                            if size:
                                ls = {
                                    "size": size.thick,
                                    "url": row.photoaddress
                                }
                                if row.description:
                                    ls["description"] = row.description
                                details.append(ls)
                                seen_photos.add(row.photoaddress)

                return Response(
                    json.dumps(details, ensure_ascii = False).encode('utf-8'),
                    content_type = 'application/json; charset=utf-8'
                )

            except Exception as e:
                return str(e)

    @jwt_required()
    def remove_granite_photos(self, url):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        query = granitephoto.query.filter(granitephoto.photoaddress == url).first()
        if not query:
            return jsonify({ "error": "No such url found" }), 400
        response = upphoto.delete_photos(query.imagekitid)
        db.session.delete(query)
        db.session.commit()
        return response