from flask import jsonify, request, Blueprint
from flask_jwt_extended import jwt_required, current_user
from graniteandmarbles import granitemarbel
from routes.utils import allowed_file
import json

grrouter = Blueprint('graniteandmarbles', __name__)

@grrouter.route("/upload/granites&marble/photos/", methods=['POST'])
@jwt_required()
def upload_granite_photos():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    graniteData = json.loads(request.form.get("data"))
    if not graniteData or not isinstance(graniteData, list):
        return jsonify({ "error": "Invalid data format. Expected a list of tile data." }), 400
    processed_data = []
    for granites in graniteData:
        product = granites.get('product')
        granite = granites.get('granite')
        thick = granites.get('thick')
        trending = granites.get('trending')
        description = granites.get('description')
        processed_data.append(
            {
                'product': product,
                'granite': granite,
                'thick': thick,
                'trending': trending,
                'description': description
            }
        )

    file = request.files.get('up_photo')
    if not file:
        return jsonify({ "error": "No file uploaded" }), 400
    if not allowed_file(file):
        return jsonify({ "error": "File type not allowed" }), 400

    processed_data.append({ 'file': file })
    gr = granitemarbel.Marbles()
    response = gr.post_granites(processed_data)
    return response


@grrouter.route("/granite/photos/", methods = ['GET'])
def get_granite_photos():
    granite = request.args.get('granite', default = None, type = str)
    thik = request.args.get('thick', default = None, type = str)
    gr = granitemarbel.Marbles()
    response = gr.get_granites(granite = granite, thik = thik)
    return response


@grrouter.route("/delete/granite/photos/", methods = ['DELETE'])
@jwt_required()
def del_granite_photos():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    url = request.form['url']
    cls = granitemarbel.Marbles()
    response = cls.remove_granite_photos(url = url)
    return response