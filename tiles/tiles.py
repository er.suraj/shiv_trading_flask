from db.model import db,app, products, sizes, rooms, tilesphotos, productroomsize, trendingproduct
from flask import abort, Response, json, jsonify
from routes import upphoto
from flask_jwt_extended import jwt_required, current_user
from trends.trend import Trending

class Tiles:
    def get_tiles(self, product, size, room):
        with app.app_context():
            if product and product.strip():
                product = product.strip()
                is_product = products.query.filter_by(productname = product).first()
                if not is_product:
                    abort(404)

            if size and size.strip():
                size = size.strip()
                is_size = sizes.query.filter_by(sizes = size).first()
                if not is_size:
                    abort(404)

            if room and room.strip():
                room = room.strip()
                is_room = rooms.query.filter_by(roomname = room).first()
                if not is_room:
                    abort(404)

            try:
                details = []
                seen_photos = set()

                if product and size and room:
                    url = productroomsize.query.join(rooms).join(products).join(sizes).filter(
                        sizes.sizes == size, products.productname == product, rooms.roomname == room
                    ).first()

                    for row in url.tilesphotos:
                        if row.photoaddress not in seen_photos:
                            ls = {
                                "size": size,
                                "url": row.photoaddress
                            }
                            if row.description:
                                ls["description"] = row.description
                            details.append(ls)
                            seen_photos.add(row.photoaddress)

                elif not size and product and room:
                    url = productroomsize.query.join(rooms).join(products).filter(
                        products.productname == product, rooms.roomname == room
                    ).all()

                    for row in url:
                        size = sizes.query.join(productroomsize).join(tilesphotos).filter(
                            tilesphotos.prsid == row.prsid
                        ).first()
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size.sizes,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                elif not room and product and size:
                    url = productroomsize.query.join(sizes).join(products).filter(
                        products.productname == product, sizes.sizes == size
                    ).all()

                    for row in url:
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                elif not room and not size and product:
                    url = productroomsize.query.join(products).filter(
                        products.productname == product
                    ).all()

                    for row in url:
                        size = sizes.query.join(productroomsize).join(tilesphotos).filter(
                            tilesphotos.prsid == row.prsid
                        ).first()
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size.sizes,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                elif not product and size and room:
                    url = productroomsize.query.join(rooms).join(sizes).filter(
                        rooms.roomname == room, sizes.sizes == size
                    ).all()

                    for row in url:
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                elif not product and not size and room:
                    url = productroomsize.query.join(rooms).filter(
                        rooms.roomname == room
                    ).all()

                    for row in url:
                        size = sizes.query.join(productroomsize).join(tilesphotos).filter(
                            tilesphotos.prsid == row.prsid
                        ).first()
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size.sizes,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                elif not product and not room and size:
                    url = productroomsize.query.join(sizes).filter(
                        sizes.sizes == size
                    ).all()

                    for row in url:
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                elif not product and not room and not size:
                    url = productroomsize.query.all()

                    for row in url:
                        size = sizes.query.join(productroomsize).join(tilesphotos).filter(
                            tilesphotos.prsid == row.prsid
                        ).first()
                        for photo in row.tilesphotos:
                            if photo.photoaddress not in seen_photos:
                                ls = {
                                    "size": size.sizes,
                                    "url": photo.photoaddress
                                }
                                if photo.description:
                                    ls["description"] = photo.description
                                details.append(ls)
                                seen_photos.add(photo.photoaddress)

                return Response(
                    json.dumps(details, ensure_ascii = False).encode('utf-8'),
                    content_type = 'application/json; charset=utf-8'
                    )

            except Exception as e:
                return str(e)

    @jwt_required()
    def post_tiles(self, dataList):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401

        file_data = dataList.pop()
        file = file_data.get('file')
        url = upphoto.upload_photo(file, "tiles")
        image_url = url["url"]
        file_id = url["file_id"]
        photo_address = image_url

        results = []
        with app.app_context():
            for data in dataList:
                product = data.get('product')
                size = data.get('size')
                room = data.get('room')
                trend = data.get('trending')
                description = data.get('description')

                try:
                    # Validate product, size, room
                    for item, model, attr in [(product, products, 'productname'), (size, sizes, 'sizes'),
                                              (room, rooms, 'roomname')]:
                        query = model.query.filter_by(**{ attr: item }).first()
                        if not query:
                            raise ValueError(f"{item} not found")

                    tile_type = productroomsize.query.join(products).join(sizes).join(rooms).filter(
                        products.productname == product,
                        rooms.roomname == room,
                        sizes.sizes == size
                    ).first()

                    if not tile_type:
                        raise ValueError("Combination not found")

                    upload_photo = tilesphotos(
                        photoaddress = photo_address, prsid = tile_type.prsid, imagekitid = file_id,
                        description = description
                        )
                    db.session.add(upload_photo)
                    db.session.commit()

                    result = { "message": "Photo uploaded successfully", "product": product, "size": size,
                               "room": room }
                    if trend == "True":
                        trending_product = Trending()
                        trend_response = trending_product.post_trending_products(photourl = photo_address)
                        result["trending"] = trend_response

                    results.append(result)

                except Exception as e:
                    results.append({ "error": str(e), "product": product, "size": size, "room": room })

        return jsonify(results)

    @jwt_required()
    def remove_tiles_photos(self, url):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        query = tilesphotos.query.filter(tilesphotos.photoaddress == url).first()
        if not query:
            return jsonify({ "error": "No such url found" }),400
        response = upphoto.delete_photos(query.imagekitid)
        db.session.delete(query)
        db.session.commit()
        return response