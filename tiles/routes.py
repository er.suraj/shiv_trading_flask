from flask import jsonify, request, Blueprint
from flask_jwt_extended import jwt_required, current_user
from routes.utils import allowed_file
from tiles import tiles
import json

tirouter = Blueprint('tiles', __name__)

@tirouter.route('/upload/tiles/photos/', methods=['POST'])
@jwt_required()
def upload_photo():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    tileData = json.loads(request.form.get("data"))
    if not tileData or not isinstance(tileData, list):
        return jsonify({ "error": "Invalid data format. Expected a list of tile data." }), 400
    processed_data = []
    for tile_data in tileData:
        product = tile_data.get('product')
        size = tile_data.get('size')
        room = tile_data.get('room')
        trending = tile_data.get('trending')
        description = tile_data.get('description')
        processed_data.append(
            {
                'product': product,
                'room': room,
                'size': size,
                'trending': trending,
                'description': description
            }
        )
    # product = request.args.get('product', type = str)
    # size = request.args.get('size', type = str)
    # room = request.args.get('room', type = str)
    # trending = request.args.get('trending', type=str)
    # description = request.form['description']
    file = request.files.get('up_photo')
    if not file:
        return jsonify({ "error": "No file uploaded" }), 400
    if not allowed_file(file):
        return jsonify({ "error": "File type not allowed" }), 400

    processed_data.append({ 'file': file })
    tile = tiles.Tiles()
    response = tile.post_tiles(processed_data)
    return response


@tirouter.route("/tiles/photos/", methods = ['GET'])
def get_tiles_photos():
    product = request.args.get('product', default = None, type = str)
    size = request.args.get('size', default = None, type = str)
    room = request.args.get('room', default = None, type = str)
    tile = tiles.Tiles()
    response = tile.get_tiles(product = product, size = size, room = room)
    return response

@tirouter.route("/delete/tiles/photos/", methods = ['DELETE'])
@jwt_required()
def del_tiles_photos():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    url = request.form['url']
    tyl = tiles.Tiles()
    response = tyl.remove_tiles_photos(url = url)
    return response