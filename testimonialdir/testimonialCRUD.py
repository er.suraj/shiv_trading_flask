from db.model import db,app, testimonial, gallery,AutoIncrementError
from flask import abort, Response, json, jsonify
from routes import upphoto
from flask_jwt_extended import jwt_required, current_user
from sqlalchemy.exc import OperationalError

class Testimonial:
    def __init__(self,file = None, description = None):
        self.file = file
        self.description = description


    def get_testimonials(self):
        with app.app_context():
            testimonialData = testimonial.query.all()
            try:
                if (testimonialData):
                    details = []
                    for row in testimonialData:
                        ls = {
                            "url": row.photoaddress
                        }
                        if row.description:
                            ls["description"] = row.description
                        details.append(ls)
                    return Response(json.dumps(details, ensure_ascii=False).encode('utf-8'), content_type='application/json; charset=utf-8')
                abort(404)
            except Exception as e:
                return str(e)

    @jwt_required()
    def post_testimonial(self):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        description = self.description
        file = self.file
        url = upphoto.upload_photo(file, "testimonial")
        image_url = url["url"]
        file_id = url["file_id"]
        photo_address = image_url
        with app.app_context():
            try:
                upload_photo = testimonial(photoaddress = photo_address, imagekitid = file_id, description = description)
                db.session.add(upload_photo)
                db.session.commit()
                return "Photo uploaded successfully"
            except OperationalError as e:
                error_message = str(e.orig)
                if 'Failed to read auto-increment value from storage engine' in error_message:
                    raise AutoIncrementError(str(e))
            except Exception as e:
                return jsonify({"error":f"{str(e)}"}), 500

    @jwt_required()
    def remove_testimonial_photos(self, url):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        query = testimonial.query.filter(testimonial.photoaddress == url).first()
        if not query:
            return jsonify({ "error": "No such url found" }),400
        response = upphoto.delete_photos(query.imagekitid)
        db.session.delete(query)
        db.session.commit()
        return response
    
    # for gallery collections

    def get_gallery(self):
        with app.app_context():
            testimonialData = gallery.query.all()
            try:
                if (testimonialData):
                    details = []
                    for row in testimonialData:
                        ls = {
                            "url": row.photoaddress
                        }
                        details.append(ls)
                    return Response(json.dumps(details, ensure_ascii=False).encode('utf-8'), content_type='application/json; charset=utf-8')
                abort(404)
            except Exception as e:
                return str(e)

    @jwt_required()
    def post_gallery(self):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        file = self.file
        url = upphoto.upload_photo(file, "gallery")
        image_url = url["url"]
        file_id = url["file_id"]
        photo_address = image_url
        with app.app_context():
            try:
                upload_photo = gallery(photoaddress = photo_address, imagekitid = file_id)
                db.session.add(upload_photo)
                db.session.commit()
                return "Photo uploaded successfully"
            except OperationalError as e:
                error_message = str(e.orig)
                if 'Failed to read auto-increment value from storage engine' in error_message:
                    raise AutoIncrementError(str(e))
            except Exception as e:
                return jsonify({"error":f"{str(e)}"}), 500

    @jwt_required()
    def remove_gallery_photos(self, url):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        query = gallery.query.filter(gallery.photoaddress == url).first()
        if not query:
            return jsonify({ "error": "No such url found" }),400
        response = upphoto.delete_photos(query.imagekitid)
        db.session.delete(query)
        db.session.commit()
        return response