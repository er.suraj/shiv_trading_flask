from flask import jsonify, request, Blueprint
from flask_jwt_extended import jwt_required, current_user
from routes.utils import allowed_file
from testimonialdir import testimonialCRUD

testimonialouter = Blueprint('testimonialdir', __name__)

@testimonialouter.route('/new/testimonials', methods=['POST'])
@jwt_required()
def upload_testimonial_photo():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    description = request.form['description']
    file = request.files['up_photo']
    if allowed_file(file):
        testimonial = testimonialCRUD.Testimonial(file = file,description = description)
        response = testimonial.post_testimonial()
        return response


@testimonialouter.route("/testimonials/", methods = ['GET'])
def get_testimonial_photos():
    testimonial = testimonialCRUD.Testimonial()
    response = testimonial.get_testimonials()
    return response

@testimonialouter.route("/delete/testimonial/", methods = ['DELETE'])
@jwt_required()
def del_testimonial_photos():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    url = request.form['url']
    testimonial = testimonialCRUD.Testimonial()
    response = testimonial.remove_testimonial_photos(url = url)
    return response

@testimonialouter.route('/gallery', methods=['POST'])
@jwt_required()
def upload_gallery_photo():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    file = request.files['up_photo']
    if allowed_file(file):
        testimonial = testimonialCRUD.Testimonial(file = file)
        response = testimonial.post_gallery()
        return response

@testimonialouter.route("/gallery/", methods = ['GET'])
def get_gallery_photos():
    testimonial = testimonialCRUD.Testimonial()
    response = testimonial.get_gallery()
    return response

@testimonialouter.route("/delete/gallery/", methods = ['DELETE'])
@jwt_required()
def del_gallery_photos():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    url = request.args.get('url')
    testimonial = testimonialCRUD.Testimonial()
    response = testimonial.remove_gallery_photos(url = url)
    return response
