from db.model import db,app, homepages
from flask import abort, Response, json, jsonify
from routes import upphoto
from flask_jwt_extended import jwt_required, current_user

class Home:
    def __init__(self, photo = None, url = None):
        self.photo = photo
        self.url = url

    @jwt_required()
    def add_homepage_image(self):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400

        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 403

        image_list = homepages.query.all()
        if len(image_list) >= 3:
            return jsonify({ "error": "Limit reached (3). Please delete previous images to add new ones." }), 400

        try:
            # Assuming 'self.photo' is validated to be present and correct
            url = upphoto.upload_photo(self.photo, "homepage")
            image_url = url.get("url")
            file_id = url.get("file_id")

            if not image_url or not file_id:
                raise ValueError("Failed to get the image URL or file ID from the upload service.")

            photo_address = image_url
            with app.app_context():
                upload_photo = homepages(photoaddress = photo_address, imagekitid = file_id)
                db.session.add(upload_photo)
                db.session.commit()
                return jsonify({ "message": "Photo uploaded successfully" }), 200
        except Exception as e:
            # Log the exception e
            return jsonify({ "error": "An error occurred during the upload process" }), 500

    @jwt_required()
    def delete_homepage_image(self):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        query = homepages.query.filter(homepages.photoaddress == self.url).first()
        if not query:
            return jsonify({ "error": "No such url found" }), 400
        response = upphoto.delete_photos(query.imagekitid)
        db.session.delete(query)
        db.session.commit()
        return response

    def get_homepage_image(self):
        try:
            photos = homepages.query.all()
            if len(photos) == 0:
                return jsonify({"Not Found":"No homepage photos added  yet"}), 404
            photo_urls = []
            for photo in photos:
                photo_urls.append(photo.photoaddress)
            return jsonify({ "photoaddress": photo_urls }), 200
        except Exception as e:
            # Log the exception here
            return jsonify({ "error": f"An error occurred while fetching homepage images:{str(e)}" }), 500