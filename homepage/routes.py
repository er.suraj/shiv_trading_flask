from flask import jsonify, request, Blueprint
from flask_jwt_extended import jwt_required, current_user
from homepage import homepage
from routes.utils import allowed_file

home = Blueprint("homepage",__name__)

@home.route("/homepage/images/", methods = ['POST'])
@jwt_required()
def upload_homepage_image():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    file = request.files["up_photo"]
    if allowed_file(file = file):
        gr = homepage.Home(photo = file)
        response = gr.add_homepage_image()
        return response

@home.route("/homepage/image/", methods = ['DELETE'])
@jwt_required()
def delete_homepage_image():
    user = current_user
    if not user:
        return jsonify({ "error": "User doesn't exist" }), 400
    if not user.issuperuser:
        return jsonify({ "error": "User not authorized to modify" }), 401
    url = request.form['url']
    cls = homepage.Home(url = url)
    response = cls.delete_homepage_image
    return response

@home.route("/images/", methods = ['GET'])
def get_homepage_image():
    cls = homepage.Home()
    response = cls.get_homepage_image()
    return response