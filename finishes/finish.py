from db.model import db, app, basicfinish, standardfinish, premiumfinish
from flask import Response, json, jsonify
from flask_jwt_extended import jwt_required, current_user
from routes import upphoto


class Finishing:
    def __init__(self, plan:str, description = None):
        self.plan = plan.upper()
        self.description = description

    @jwt_required()
    def get_finish_photos(self):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        table = ""
        if not user.issuperuser:
            if self.plan == "BASIC":
                table = basicfinish
            elif self.plan == "STANDARD":
                table = standardfinish
            elif self.plan == "PREMIUM":
                table = premiumfinish
            query = table.query.all()
            finish = []
            if query:
                for row in query:
                    ls = {
                        "url":row.photoaddress
                    }
                    if row.description:
                        ls["description"]= row.description
                    finish.append(ls)
                return Response(
                    json.dumps(finish, ensure_ascii = False).encode('utf-8'),
                    content_type = 'application/json; charset=utf-8'
                )
            else:
                return jsonify({"error":f"Photos for {user.plan} finishes not found"}), 400
        elif user.issuperuser:
            plans = ["basic","standard","premium"]
            finish = []
            for plan in plans:
                if plan == "basic":
                    table = basicfinish
                elif plan == "standard":
                    table = standardfinish
                elif plan == "premium":
                    table = premiumfinish
                rows = []
                query = table.query.all()
                if query:
                    for row in query:
                        ls = {
                            "url":row.photoaddress
                        }
                        if row.description:
                            ls["description"]= row.description
                        rows.append(ls)
                finish.append({"plan":plan,"urls":rows})
            if len(finish)<=0:
                    return jsonify({"error":f"Photos for any of the finishes not found"}), 400
            else:
                return Response(
                        json.dumps(finish, ensure_ascii = False).encode('utf-8'),
                        content_type = 'application/json; charset=utf-8'
                    )
        # elif self.plan == "STANDARD":
        #     query = standardfinish.query.all()
        #     finish = []
        #     if query:
        #         for row in query:
        #             ls = {
        #                 "url": row.photoaddress
        #             }
        #             if row.description:
        #                 ls["description"] = row.description
        #             finish.append(ls)
        #         return Response(
        #             json.dumps(finish, ensure_ascii = False).encode('utf-8'),
        #             content_type = 'application/json; charset=utf-8'
        #         )
        #     else:
        #         return jsonify({ "error": "Photos for standard finishes not found" }), 400
        # elif self.plan == "PREMIUM":
        #     query = premiumfinish.query.all()
        #     finish = []
        #     if query:
        #         for row in query:
        #             ls = {
        #                 "url": row.photoaddress
        #             }
        #             if row.description:
        #                 ls["description"] = row.description
        #             finish.append(ls)
        #         return Response(
        #             json.dumps(finish, ensure_ascii = False).encode('utf-8'),
        #             content_type = 'application/json; charset=utf-8'
        #         )
        #     else:
        #         return jsonify({"error":"Photos for premium finishes not found"}), 400
        # return jsonify({"error":"Admin is in the process of adding photos"}), 404

    @jwt_required()
    def post_finish_photos(self, file):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        plan_list = ['BASIC', 'STANDARD', 'PREMIUM']
        if self.plan.upper() not in plan_list:
            return jsonify({'error':'No such plan exists'}),400
        with app.app_context():
            description = self.description
            if self.plan.upper() == "BASIC":
                url = upphoto.upload_photo(file, "basic_finish")
                image_url = url["url"]
                file_id = url["file_id"]
                finish = basicfinish(photoaddress = image_url, imagekitid = file_id, description = description)
                db.session.add(finish)
                db.session.commit()
                db.session.refresh(finish)
                return "Uploaded photo of basic finish category"
            if self.plan.upper() == "STANDARD":
                url = upphoto.upload_photo(file, "standard_finish")
                image_url = url["url"]
                file_id = url["file_id"]
                finish = standardfinish(photoaddress = image_url, imagekitid = file_id, description = description)
                db.session.add(finish)
                db.session.commit()
                db.session.refresh(finish)
                return "Uploaded photo of standard finish category"
            if self.plan.upper() == "PREMIUM":
                url = upphoto.upload_photo(file, "premium_finish")
                image_url = url["url"]
                file_id = url["file_id"]
                finish = premiumfinish(photoaddress = image_url, imagekitid = file_id, description = description)
                db.session.add(finish)
                db.session.commit()
                db.session.refresh(finish)
                return "Uploaded photo of premium finish category"
            
    @jwt_required()
    def remove_finish_photos(self, url):
        user = current_user
        if not user:
            return jsonify({ "error": "User doesn't exist" }), 400
        if not user.issuperuser:
            return jsonify({ "error": "User not authorized to modify" }), 401
        plan_list = ['BASIC', 'STANDARD', 'PREMIUM']
        if self.plan.upper() not in plan_list:
            return jsonify({'error':'No such plan exists'}),400
        elif self.plan == "BASIC":
            query = basicfinish.query.filter(basicfinish.photoaddress == url).first()
            if not query:
                return jsonify({ "error": f"No such url found for {self.plan} subscription."}),400
            response = upphoto.delete_photos(query.imagekitid)
            db.session.delete(query)
            db.session.commit()
            return response
        elif self.plan == "STANDARD":
            query = standardfinish.query.filter(standardfinish.photoaddress == url).first()
            if not query:
                return jsonify({ "error": f"No such url found for {self.plan} subscription."}),400
            response = upphoto.delete_photos(query.imagekitid)
            db.session.delete(query)
            db.session.commit()
            return response
        elif self.plan == "PREMIUM":
            query = premiumfinish.query.filter(premiumfinish.photoaddress == url).first()
            if not query:
                return jsonify({ "error": f"No such url found for {self.plan} subscription."}),400
            response = upphoto.delete_photos(query.imagekitid)
            db.session.delete(query)
            db.session.commit()
            return response